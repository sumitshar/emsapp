package com.pushkal.ems.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.pushkal.ems.dao.EmployeeRepository;
import com.pushkal.ems.model.Employee;
import com.pushkal.ems.model.EmployeeDTO;

@Service
public class EmployeeService implements UserDetailsService{

	@Autowired
	private EmployeeRepository employeeRepos;
	@Autowired
	private PasswordEncoder bcryptEncoder;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		Employee e = employeeRepos.findByUsername(username);
		if(e==null) {
			throw new UsernameNotFoundException("Employee not found with username:" + username);
		}
		return new org.springframework.security.core.userdetails.User(e.getUsername(),e.getPassword(),new ArrayList<>());
	}
	
	public Employee save(EmployeeDTO es) {
		Employee e = new Employee();
		e.setUsername(es.getUsername());
		e.setPassword(bcryptEncoder.encode(es.getPassword()));
		e.setName(es.getName());
		e.setAddress(es.getAddress());
		e.setContact(es.getContact());
		return employeeRepos.save(e);
		
	}
	
	public List<Employee> listOfEmployees(){
		
		return (List<Employee>) employeeRepos.findAll();
	}
}
