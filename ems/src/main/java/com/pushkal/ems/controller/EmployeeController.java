package com.pushkal.ems.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pushkal.ems.model.Employee;
import com.pushkal.ems.service.EmployeeService;

@RestController
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;
	
	@RequestMapping({ "/employees" })
	public List<Employee> employeeList() {
		return employeeService.listOfEmployees();
	}
	
	@GetMapping("/message")
	public String applicationStatus()
	{
		return "You Are Authorized TO Access All Apis Now Because You Are Logged In ";
	}

}