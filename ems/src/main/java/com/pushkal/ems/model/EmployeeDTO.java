package com.pushkal.ems.model;

import javax.persistence.Column;

public class EmployeeDTO {
	private String name;
	private String username;
	private String password;
	private String contact;
	private String address;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public EmployeeDTO(String name, String username, String password, String contact, String address) {
		super();
		this.name = name;
		this.username = username;
		this.password = password;
		this.contact = contact;
		this.address = address;
	}
	public EmployeeDTO() {
		super();
		// TODO Auto-generated constructor stub
	}


}
