package com.pushkal.ems.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.pushkal.ems.model.Employee;
@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Integer> {
	
	public Employee findByUsername(String username);
}
