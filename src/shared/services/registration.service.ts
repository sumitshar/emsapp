import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {
url:string;
  constructor(private _http:HttpClient) {
   
    this.url="http://localhost:8080/"
     
   }

   
   public addEmployee(regs:any):Observable<any>{
    return this._http.post ("http://localhost:8080/"+"register",regs);
   }



}
