import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  url:string;
  constructor(private _http1:HttpClient) { 
    this.url="http://localhost:8080/"
  }
  public findEmployee(log:any):Observable<any>{
    return this._http1.post("http://localhost:8080/"+"login",log);
  }
}
