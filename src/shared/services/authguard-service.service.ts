import { Injectable } from '@angular/core';
import { Employee } from '../interfaces/employee';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthguardServiceService {

  constructor(private router1:Router) { }

  login(employee:Employee) {  
    if (employee!=null) {  
      alert("Login SuccessFUlly Done");
      sessionStorage.setItem('currentUser', "loggedin");
      sessionStorage.setItem('name',employee.fName);
      this.router1.navigate(["/home"]);
        
      return true;  
    }  
  }  
  logout() {  
    sessionStorage.removeItem('currentUser');  
    sessionStorage.removeItem('name');
    

  }  
  public loggedIn(): boolean {  
    return (sessionStorage.getItem('currentUser') !== null);      
  }  
}
