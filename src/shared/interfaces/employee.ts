export interface Employee {
	fName:string;
	lName:string;
	email:string;
	password:string;
	address:string;
	contact:string;
}
