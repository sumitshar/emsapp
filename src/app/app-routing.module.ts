import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { RegistrationComponent } from './registration/registration.component';
import { LoginComponent } from './login/login.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { AuthenticationGuard } from 'src/shared/guard/authentication.guard';
import { AuthguardServiceService } from 'src/shared/services/authguard-service.service';


const routes: Routes = [
  
  {path:"home",component:HomeComponent,canActivate:[AuthenticationGuard]},
  {path:"registration",component:RegistrationComponent},
  {path:"login",component:LoginComponent},
  {path:"contact",component:ContactUsComponent,canActivate:[AuthenticationGuard]},
  {path:"",component:HomeComponent,canActivate:[AuthenticationGuard]},
  {path:"404",component:PageNotFoundComponent,canActivate:[AuthenticationGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
