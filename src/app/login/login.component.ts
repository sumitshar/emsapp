import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { LoginService } from 'src/shared/services/login.service';
import { Router } from '@angular/router';
import { AuthguardServiceService } from 'src/shared/services/authguard-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm:FormGroup
  constructor(private fb:FormBuilder,private loginService:LoginService,private auth:AuthguardServiceService) {
    this.loginForm = this.fb.group({
      email:[""],
      password:[""]
    })
   }

   onSubmit(){
     this.loginService.findEmployee(this.loginForm.value).subscribe(val=>{
      this.auth.login(val)
       
      });
    console.log(this.loginForm.value);
   }
  ngOnInit(): void {
    
  }

}
