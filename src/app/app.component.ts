import { Component } from '@angular/core';
import { AuthguardServiceService } from 'src/shared/services/authguard-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'EMS-app';
  user:String;
  constructor(private auth:AuthguardServiceService, private router:Router
    ){
   this.user=sessionStorage.getItem("name")
  }

  logout() {  
    this.auth.logout();  
    this.router.navigate(['login']);  
  }  

  isLoggedIn():boolean{
   return this.auth.loggedIn();
  }
}
