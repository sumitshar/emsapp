import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { RegistrationService } from 'src/shared/services/registration.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  registerForm:FormGroup
  constructor(private fb:FormBuilder,private regstrationService:RegistrationService,private router:Router) {
    this.registerForm = this.fb.group({
      fName:[""],
      lName:[""],
      email:[""],
      password:[""],
      address:[""],
      contact:[""]
    })
   }


   onSubmit(){
     this.regstrationService.addEmployee(this.registerForm.value).subscribe(val=>{
      console.log(val); 
      alert("Registration SuccessFUlly Done") ;
       this.router.navigate(["login"]);
    });
     
   }

  ngOnInit(): void {
  }

}
